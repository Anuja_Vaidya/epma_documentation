# Medicines on admission

We document the medicines a patient is currently taking in the Medicines On Admission (MOA) list. To be clear, the MOA list is not a prescription, it's just a list of medicines that the patient is currently taking that can be reffered to and used when prescribing and in generating the discharge summary.

We get to the medicines on admission list via the 'Medicines on Admission & Discharge' icon within the navigation icons:
![[Medication_reconciliation.svg]]

The MOA list is on the left:

![[Medicines On Admission Start.png]]

Select 'Start'.

Once you have confirmed you are sure - you will be able to enter medicines onto the MOA list. New medicines can be entered from the seach bar or from a pre-defined list.

## Entering a new medicine from the search bar

The search bar is displayed underneath the navigation icons and the patients reference weight. To seach you need to enter at lest the first three letters of the medicine you are looking for:

![[Entering a new medicines from the search bar.png]]

If you enter 'Amoxi' the system will retrun all the medicines that contain those characters:

![[Search.png]]

The results are displayed in the VTM, VMP, AMP hierachy (please reffer to the latest NHS documentration for more details: https://simplifier.net/guide/DigitalMedicines/Medications).

Selecting the arrow icon to the left expands the hierachy:

![[Expanded search results.png]]

In the example above only the VTM and VMPs are displayed. For this medicine the system configurator has made the AMPs non-prescribable and so they will not appear in the search results.

The user can select either the VTM or one of the VMPs. This opens the 'Posology' view:

![[Posology view.png]]

Here the route, dose, dosage interval, indications, prescription source and comments can be added.

![[Posology view completed.png]]

Select 'Add' to add the medicine to the MOA list:

![[Medicine added to MOA list from search.png]]

## Entering a medicine from a pre-defined list
Below the search bar there are three different predefined lists:
- Order sets
- My favourites
- Patient lists

### Order sets
Order sets are groups of medicines that have been pre-defined by your organisation. These will be group around commonly presctibed medicines and indications. These will typically be used for prescribing medicines and not for adding medicines to the MOA list.

### My favourites
If there are medicines that you prescribe on a regular basis - you can save them to the 'My favourites' folder. Again these would typically be used for prescribing medicines and not for adding medicines to the MOA list.

### Patient lists
The patient lists folder provides lists of medicines that have been created for the patient in the past, incuding previous MOA lists and Discharge Presctiptions. This list may also show lists of medicines from other connected systems (e.g. primary care, or from pre-operative assessment) if they are available. These lists may prove very helpful in creating the MOA lists.

## Saving the MOA list
Once all the medicines have been added select the 'Save' button to save the MOA list.

![[MOA list Save.png]]

The MOA list is now displayed on the left of the 'Medicines on Admission and Discharge' view. More medicines can be added by selecting "Edit" that has now replaced "Start".
 
## Patients Own Drugs
If the patient has brought in a supply of medicine that has been added to the MOA list - this infomration can be added in the "Patient Drugs" view. To bring this up, select the "Patient Own Drugs" menu option for that medicine in the MOA list:

![[images/application screenshots/Patients Own Drugs menu.png]]

The Patient durgs view allows you to enter the quantity of medicine, if the patient has a compliance aid, if the patient has their own supply at home and where the medicine should be resupplied from should that be required.

![[Patient Drugs.png]]


## Comments
Additional comments about any MOA medicine can be added via the comments and review icon:
![[Pharmacist review not required.svg]]

## Notes
Selecting "Notes" in the MOA list allows the addition of free text notes.


## Marking the MOA list as complete




