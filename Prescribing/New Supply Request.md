        
Upon selection from menu item Supply Request and IF **NO** Supply Request record exists, then system will display a modal window with option to create new Supply Request.

#### Rules for medication to create supply Request

· IF the prescribed medication is VTM, then system will provide the ability for user select a VMP/AMP. For VTM, Supply Request cannot be created.

· IF the prescribed medication is VMP/AMP, then system will provide the ability for user to select different VMP/AMP

· Supply Request will have several statuses

o **Incomplete-** ![[Incomplete]]

o **Pending-** ![[Pending]]

o **Approved-** ![[Approved]]

o **Fulfilled-** ![[Fulfilled]]

o **Rejected-** ![[Rejected]]
