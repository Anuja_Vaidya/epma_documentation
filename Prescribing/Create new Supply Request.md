1. Select an option **Supply->Supply Request** from the hamburger of the prescription.
2. The new modal window will be open with, 
	1. IF **NO** Supply Request record exists, then system will display a modal window with option to create new Supply Request. (Fig.1)
	2. IF supply request records **DO** exist for the current encounter with a status =       Pending or Approved or Fulfilled or Rejected, THEN the system will display the           following screen (Fig.2)
3. IF its a new Supply request, select option 'New', and modal window will open with options,




	