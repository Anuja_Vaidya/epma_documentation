 

When needed with maximum dose
Antimicrobial
Titration
Medicinal gas
Infusion / Continuous infusion

→ Dose related scenarios Stat. dose

Stat. dose
Variable dose
Variable/protocol
Chosen day
By doctor's orders
Descriptive dose
Dosing per weight
Dosing per surface area
Modified - release
Dose range

→ Infusion related scenario Injections

Infusion / Injection
Infusion / Bolus
Infusion / Rate
Infusion / Rate / Variable
Infusion / Linked
Continuous infusion / Variable
Infusion / Blood produt
Parenteral nutrition
Free entry

 

→ Order set

Order set creation
Adding prescription to existing order set
Adding multiple prescriptions to the order set




→ How to
 

How to set a reminder?
 

How to edit prescriptions on the Therapy list?
 

How to remove prescriptions from the Therapy list?
 

How to review active warnings on the Therapy List?
 

How to add a source of the prescription?


 

Therapy Overview
 

Prescription management

 

How to edit therapy?
 

How to suspend/pause a therapy?
 

How to stop a therapy?
 

How to copy a therapy?
 

How to find additional information about a medication?
 

How to review  
a pharmacist report?
 

How to see the history of a prescription?
 

How to define the doses for titration prescriptions?

 

How to confirm or cancel administration for “By doctor’s orders” prescriptions?

 

How to stop all prescriptions with one action?

 

How to suspend prescriptions due to temporary leave

 

How to record additional administrations?

 

How to order a resupply of a medication

 

How to change infusion rate?

 

How to pause flow rate for infusions or medicinal gases?

 

How to add a bolus administration for infusions?

 

How to change the device for medicinal gases?

 

How to record a change of a bag or infusion set?

 

How to find stopped prescriptions?