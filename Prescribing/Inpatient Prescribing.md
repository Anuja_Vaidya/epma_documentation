
To prescribe a medcine in openEPMA click/tap on the prescribe icon: ![[images/Prescribe_medication.svg]]


Navigational menu Medication
Patient data bar

Below the navigation bar lies a patient data bar which includes the patient’s name, weight with the date of the measurement, and information about possible allergies.

Prescription list
Prescription card
Medication timeline
Filtering, grouping, sorting od medications No grouping




##Prescribing
 

Prescribing medications

The prescribing form is divided into three parts. On the left-hand side there are three possibilities for medication prescribing: smart search, order set, or universal form. On the right-hand side there is a Therapy List, which is a list of prospective medications, and a Warnings Section with all the applicable warnings, such as medication contraindication.

Universal form 
Order sets
Therapy list
Warnings section
Smart search 
Order sets  
Universal form 
Therapy list  
Warnings


###Prescribing form

 

Prescribing form

Prescribe medications
Routes of administration
Dosing interval options
Therapy duration specifications
Snippet of a timeline 
Additional information
Dose related fields
Additional dosing instructions
When needed & By doctor's orders
Expanded
Add to order set
Show source
Add
Dose related fields Prescribe medications
Clear Expanded Add to order set
Variable ...
Dosing interval Prescribe medications
Therapy start and duration specifications Prescribe medications
Snippet of a timeline
Additional information Prescribe medications