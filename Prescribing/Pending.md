- Pending - always available for selection:
- Selected by default if a supply request, for a formulary or non-formulary request, with a status of Incomplete was save        
- IF current status = ‘Approved’ (for a formulary or non-formulary request), THEN the User will only be able to select ‘Pending’ IF the User has the RBAC role to change the status of a request from ‘Approved' to 'Pending’