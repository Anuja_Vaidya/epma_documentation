-   Approved: always available for selection:
    
-   Selected by default if a supply request, for a formulary or non-formulary request, was previously saved as ‘Approved’
        
-   The User will only be able to select ‘Approved’ IF the User has been assigned the RBAC role(s) to approve a formulary or non-formulary request:
        
 -   IF current status = ‘Fulfilled’, THEN the User will only be able to select ‘Approved’ IF the User has the ![[RBAC]] role to change the status of a request from ‘Fulfilled' to 'Approved’