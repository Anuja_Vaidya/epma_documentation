## Navigating to the openEPMA module
To use the openEPMA module you must first launch the Interneuron Modular Care Record. The Modular Care Record is a web based application an the link will be provided to you by your organisation (it may also appear as an icon on your organisation desktop or device home screen).

![[terminus patient specific view.png]]

Launchin the Interneuron Modular Care Record takes you to the patient specific view. This will (by default) take you to one of the Modules. If it is not the openEPMA module you will need to select it from the drop down module selector:

![[module selector.png]]

Select the openEPMA module from the module selector.