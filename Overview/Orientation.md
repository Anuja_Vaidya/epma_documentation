## Orientation
The application has three main sections. The patient banner, the patient list and the module itself.

### The patient banner
![[highlighted patient banner.png]]

The patient banner is awlays displayed - reguarless of which module is being used. This is intended to ensure that you are always aware of which patient you have selected.

The banner shows:
 - The patient name, age, date of birth and unique identifiers (NHS and hospital numbers)
 - Location (if they are admitted)
 - Allergies
 - Banner warnings
 - Status badges

The banner can be expanded by using the icon on the far right of the banner: ![[expand patient banner icon.png]] this will display any additional information that is available, such as GP details.

![[expanded banner.png]]

### The patient list
![[highlighted patient list.png]]

The patient list displays all the patients for a specific team, ward or clinic. This list can be configured to display the group of patient you need to see, but will default to a group (e.g. ward) that will be set up for you in advance.

### The openEPMA module
![[highlighted module.png]]

The module displays the list of prescribed medicines at the centre:
![[highlighted list of prescribed medicines.png]]

At the top are the navigation icons:

![[highlighted navigation icon.png]]

To the right of the screen are the medicine grouping preferences:
![[highlighted medicines grouping preferences.png]]

And the encounter selector is below the grouping preferences:
![[encounter selector.png]]
