 

This user guide is for the Interneuron openEPMA module, a module of the Interneuron Modular Care Record. It will guide you though using openEPMA to (among other things) safely prescribe and administer medicines.